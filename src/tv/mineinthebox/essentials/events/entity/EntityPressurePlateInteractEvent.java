package tv.mineinthebox.essentials.events.entity;

import java.util.HashSet;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityInteractEvent;

public class EntityPressurePlateInteractEvent implements Listener {
	
	private final HashSet<Material> pressure_plates = new HashSet<Material>() {{
		add(Material.ACACIA_PRESSURE_PLATE);
		add(Material.BIRCH_PRESSURE_PLATE);
		add(Material.DARK_OAK_PRESSURE_PLATE);
		add(Material.HEAVY_WEIGHTED_PRESSURE_PLATE);
		add(Material.JUNGLE_PRESSURE_PLATE);
		add(Material.LIGHT_WEIGHTED_PRESSURE_PLATE);
		add(Material.OAK_PRESSURE_PLATE);
		add(Material.SPRUCE_PRESSURE_PLATE);
		add(Material.STONE_PRESSURE_PLATE);
	}};
	
	@EventHandler
	public void onInteract(EntityInteractEvent e) {
		
		if(e.isCancelled()) {
			return;
		}
		
		if(e.getEntity() instanceof LivingEntity && e.getEntity().getType() != EntityType.PLAYER) {
			if(pressure_plates.contains(e.getBlock().getType())) {
				e.setCancelled(true);	
			}
		}
	}

}
