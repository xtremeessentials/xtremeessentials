package tv.mineinthebox.essentials.events.entity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import tv.mineinthebox.essentials.xEssentials;

public class SpawnEggLogEvent implements Listener {
	
	private final HashMap<Material, String> idNames = new HashMap<Material, String>();
    private final xEssentials pl;
    
    public SpawnEggLogEvent(xEssentials pl) {
    	this.pl = pl;
    }
    
	@SuppressWarnings("deprecation")
	@EventHandler
	public void throwEggLog(PlayerInteractEvent e) {
		if(idNames.isEmpty()) {
			setHashMap();
		}
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getPlayer().getItemInHand().getType() == Material.LEGACY_MONSTER_EGG || idNames.containsKey(e.getPlayer().getItemInHand().getType())) {
				Calendar cal = Calendar.getInstance();
				cal.getTime();
				SimpleDateFormat sdf = new SimpleDateFormat("[HH:mm] E dd/MM/yy z - HH:mm:ss");
				try {
					FileWriter fw = new FileWriter(pl.getDataFolder() + File.separator + "spawnEgg_activity.log", true);
					w(fw,  sdf.format(cal.getTime()) + " " + e.getPlayer().getName() + " tried to throw " + idNames.get(e.getPlayer().getItemInHand().getType().name()) + " in the world " + e.getPlayer().getWorld().getName());
					fw.close();
				} catch(Exception a) {
					a.printStackTrace();
				}
			}
		}
	}

	private void w(FileWriter writer, String string) throws IOException {
		writer.write(string + "\n");
	}

	//TODO: fix spawnegg logging
	public void setHashMap() {
		idNames.put(Material.BLAZE_SPAWN_EGG, "Blaze_Spawn_Egg");
		idNames.put(Material.CAVE_SPIDER_SPAWN_EGG, "Cave_Spider_Spawn_Egg");
		idNames.put(Material.CREEPER_SPAWN_EGG, "Creeper_Spawn_Egg");
		idNames.put(Material.ENDERMAN_SPAWN_EGG, "Enderman_Spawn_Egg");
		idNames.put(Material.GHAST_SPAWN_EGG, "Ghast_Spawn_Egg");
		idNames.put(Material.MAGMA_CUBE_SPAWN_EGG, "Magma_Cube_Spawn_Egg");
		idNames.put(Material.SILVERFISH_SPAWN_EGG, "SilverFish_Spawn_Egg");
		idNames.put(Material.SKELETON_SPAWN_EGG, "Skeleton_Spawn_Egg");
		idNames.put(Material.SLIME_SPAWN_EGG, "Slime_Spawn_Egg");
		idNames.put(Material.SPIDER_SPAWN_EGG, "Spider_Spawn_Egg");
		idNames.put(Material.WITCH_SPAWN_EGG, "Witch_Spawn_Egg");
		idNames.put(Material.ZOMBIE_SPAWN_EGG, "Zombie_Spawn_Egg");
		idNames.put(Material.ZOMBIE_PIGMAN_SPAWN_EGG, "Zombie_Pig_Spawn_Egg");
		idNames.put(Material.BAT_SPAWN_EGG, "Bat_Spawn_Egg");
		idNames.put(Material.CHICKEN_SPAWN_EGG, "Chicken_Spawn_Egg");
		idNames.put(Material.COW_SPAWN_EGG, "Cow__SpawnEgg");
		idNames.put(Material.HORSE_SPAWN_EGG, "Horse_Spawn_Egg");
		idNames.put(Material.MOOSHROOM_SPAWN_EGG, "Mooshroom_Spawn_Cow_Egg");
		idNames.put(Material.OCELOT_SPAWN_EGG, "Ocelot_Spawn_Egg");
		idNames.put(Material.PIG_SPAWN_EGG, "Pig_Spawn_Egg");
		idNames.put(Material.SHEEP_SPAWN_EGG, "Sheep_Spawn_Egg");
		idNames.put(Material.SQUID_SPAWN_EGG, "Squid_Spawn_Egg");
		idNames.put(Material.WOLF_SPAWN_EGG, "Wolf_Spawn_Egg");
		idNames.put(Material.VILLAGER_SPAWN_EGG, "Villager_Spawn_Egg");
		
		idNames.put(Material.DRAGON_EGG, "Dragon_Egg");
		idNames.put(Material.TURTLE_EGG, "Turtle_Egg");
		idNames.put(Material.COD_SPAWN_EGG, "COD_Spawn_Egg");
		idNames.put(Material.DOLPHIN_SPAWN_EGG, "Dolphin_Spawn_Egg");
		idNames.put(Material.DONKEY_SPAWN_EGG, "Donkey_Spawn_Egg");
		idNames.put(Material.DROWNED_SPAWN_EGG, "Drowned_Spawn_Egg");
		idNames.put(Material.ELDER_GUARDIAN_SPAWN_EGG, "ElderGuardian_Spawn_Egg");
		idNames.put(Material.ENDERMITE_SPAWN_EGG, "Endermite_Spawn_Egg");
		idNames.put(Material.EVOKER_SPAWN_EGG, "Evoker_Spawn_Egg");
		idNames.put(Material.GUARDIAN_SPAWN_EGG, "Guardian_Spawn_Egg");
		idNames.put(Material.HUSK_SPAWN_EGG, "Husk_Spawn_Egg");
		idNames.put(Material.LLAMA_SPAWN_EGG, "Llama_Spawn_Egg");
		idNames.put(Material.MULE_SPAWN_EGG, "Mule_Spawn_Egg");
		idNames.put(Material.PARROT_SPAWN_EGG, "Parrot_Spawn_Egg");
		idNames.put(Material.PHANTOM_SPAWN_EGG, "Phantom_Spawn_Egg");
		idNames.put(Material.POLAR_BEAR_SPAWN_EGG, "Polarbear_Spawn_Egg");
		idNames.put(Material.PUFFERFISH_SPAWN_EGG, "Pufferfish_Spawn_Egg");
		idNames.put(Material.RABBIT_SPAWN_EGG, "Rabbit_Spawn_Egg");
		idNames.put(Material.SALMON_SPAWN_EGG, "Salmon_Spawn_Egg");
		idNames.put(Material.SHULKER_SPAWN_EGG, "Shulker_Spawn_Egg");
		idNames.put(Material.SKELETON_HORSE_SPAWN_EGG, "SkeletonHorse_Spawn_Egg");
		idNames.put(Material.TROPICAL_FISH_SPAWN_EGG, "tropicalfish_Spawn_Egg");
		idNames.put(Material.STRAY_SPAWN_EGG, "Stray_Spawn_Egg");
		idNames.put(Material.TURTLE_SPAWN_EGG, "Turtle_Spawn_Egg");
		idNames.put(Material.VEX_SPAWN_EGG, "Vex_Spawn_Egg");
		idNames.put(Material.VINDICATOR_SPAWN_EGG, "Vindicator_Spawn_Egg");
		idNames.put(Material.WITHER_SKELETON_SPAWN_EGG, "Wither_Skeleton_Spawn_Egg");
		idNames.put(Material.ZOMBIE_HORSE_SPAWN_EGG, "Zombie_Horse_Spawn_Egg");
		
		
		
	}

}
