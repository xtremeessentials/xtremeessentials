package tv.mineinthebox.essentials.events.players;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.interfaces.XPlayer;

public class RemoveBiomeTPEvent implements Listener {
	
	private xEssentials pl;
	
	public RemoveBiomeTPEvent(xEssentials pl) {
		this.pl = pl;
	}
	
	@EventHandler
	public void removeBiomeEventQuit(PlayerQuitEvent e) {
		XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(e.getPlayer().getName());
		if(xp.hasBiomeTP()) {
			xp.removeBiomeTP();
		}
	}
	
	@EventHandler
	public void removeBiomeEventKick(PlayerKickEvent e) {
		XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(e.getPlayer().getName());
		if(xp.hasBiomeTP()) {
			xp.removeBiomeTP();
		}
	}

}
