package tv.mineinthebox.essentials.events.players;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.interfaces.XPlayer;

public class GodmodeEvent implements Listener {
	
	private xEssentials pl;
	
	public GodmodeEvent(xEssentials pl) {
		this.pl = pl;
	}
	
	@EventHandler
	public void onGodmodeEvent(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player)e.getEntity();
			XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
			if(xp.isGodmode()) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onGodmodeEvent(EntityDamageByEntityEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player)e.getEntity();
			XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
			if(xp.isGodmode()) {
				e.setCancelled(true);
			}
		}
	}

}
