package tv.mineinthebox.essentials.events.chat;

import org.bukkit.Particle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import tv.mineinthebox.essentials.events.customevents.PlayerChatSmilleyEvent;

public class ChatSmilleyParticleEvent implements Listener {
	
	@EventHandler
	public void onChatParticle(PlayerChatSmilleyEvent e) {
		if(e.getMessage().contains(":@")) {
			double x = e.getPlayer().getLocation().getX();
			double y = e.getPlayer().getLocation().getY();
			double z = e.getPlayer().getLocation().getZ();
			e.getPlayer().getWorld().spawnParticle(Particle.VILLAGER_ANGRY, x, y+3, z, 1, 2, 0, 2, 1, 3);
			//EffectType.playEffect(e.getPlayer().getWorld(), EffectType.VILLAGER_ANGRY, e.getPlayer().getLocation().add(0, 3, 0), 2, 0, 2, 1, 3);
		} else if(e.getMessage().contains("<3")) {
			double x = e.getPlayer().getLocation().getX();
			double y = e.getPlayer().getLocation().getY();
			double z = e.getPlayer().getLocation().getZ();
			e.getPlayer().getWorld().spawnParticle(Particle.HEART, x, y+3, z, 1, 2, 0, 2, 1, 3);
			//EffectType.playEffect(e.getPlayer().getWorld(), EffectType.HEART, e.getPlayer().getLocation().add(0, 3, 0), 2, 0, 2, 1, 3);
		}
	}

}
