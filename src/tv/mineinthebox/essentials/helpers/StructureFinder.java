package tv.mineinthebox.essentials.helpers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class StructureFinder {
	
	//credits to Ugleh from https://www.spigotmc.org/threads/how-to-find-closest-generated-structure.338532/
	
    public Location getStructure(Location l, String structure) throws Exception {
       Method getHandle = l.getWorld().getClass().getMethod("getHandle");
       Object nmsWorld = getHandle.invoke(l.getWorld());
       Object blockPositionString = nmsWorld.getClass().getMethod("a", new Class[] { String.class, getNMSClass("BlockPosition"), int.class, boolean.class }).invoke(nmsWorld, structure,getBlockPosition(l), 100,false);
       String position = blockPositionString.toString();
          
       int xstart = position.indexOf("x=")+2;
       int xend = position.indexOf(", ", xstart);
       int x = Integer.parseInt(position.substring(xstart, xend));
       
       int zstart = position.indexOf("z=")+2;
       int zend = position.lastIndexOf('}');
       int z = Integer.parseInt(position.substring(zstart, zend));
       
       Location loc = l.getWorld().getHighestBlockAt(x, z).getLocation().add(0, 1, 0);
       return loc;
   }

  
   private Class<?> getNMSClass(String nmsClassString) throws ClassNotFoundException {
        String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
        String name = "net.minecraft.server." + version + nmsClassString;
        Class<?> nmsClass = Class.forName(name);
        return nmsClass;
    }
   
   private Object getBlockPosition(Location loc) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
        Class<?> nmsBlockPosition = getNMSClass("BlockPosition");
        Object nmsBlockPositionInstance = nmsBlockPosition
                .getConstructor(new Class[] { Double.TYPE, Double.TYPE, Double.TYPE })
                .newInstance(new Object[] { loc.getX(), loc.getY(), loc.getZ() });
        return nmsBlockPositionInstance;
    }

   //end credits
   
   /*
   enum StructureType {
	   BURIED_TREASURE("Buried_Treasure"),
	   DESERT_PYRAMID("Desert_Pyramid"),
	   JUNGLE_PYRAMID("Jungle_Pyramid"),
	   ENDCITY("EndCity"),
	   FORTRESS("Fortress"),
	   IGLOO("Igloo"),
	   MANSION("Mansion"),
	   MINESHAFT("Mineshaft"),
	   MONUMENT("Monument"),
	   OCEAN_RUIN("Ocean_Ruin"),
	   SHIPWRECK("Shipwreck"),
	   STRONGHOLD("Stronghold"),
	   SWAMP_HUT("Swamp_Hut"),
	   VILLAGE("Village");
	   
	   private final String name;
	   
	   private StructureType(String name) {
		   this.name = name;
	   }
	   
	   public String getName() {
		   return name;
	   }
   }
   */

}
