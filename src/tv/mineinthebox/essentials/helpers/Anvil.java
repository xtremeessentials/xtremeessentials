package tv.mineinthebox.essentials.helpers;

import org.bukkit.Material;
import org.bukkit.block.Block;


public class Anvil {

	private final Block block;

	public Anvil(Block block) {
		this.block = block;
	}

	public enum AnvilDamageType {
		NON_DAMAGED(),
		SLIGHT_DAMAGED(),
		HEAVY_DAMAGED();
	}

	@SuppressWarnings("deprecation")
	public AnvilDamageType getAnvilDamageType() {
		byte data = block.getData();
		switch(data) {
		case 0: return AnvilDamageType.NON_DAMAGED;
		case 1: return AnvilDamageType.NON_DAMAGED;
		case 2: return AnvilDamageType.NON_DAMAGED;
		case 3: return AnvilDamageType.NON_DAMAGED;
		case 4: return AnvilDamageType.SLIGHT_DAMAGED;
		case 5: return AnvilDamageType.SLIGHT_DAMAGED;
		case 6: return AnvilDamageType.SLIGHT_DAMAGED;
		case 7: return AnvilDamageType.SLIGHT_DAMAGED;
		case 8: return AnvilDamageType.HEAVY_DAMAGED;
		case 9: return AnvilDamageType.HEAVY_DAMAGED;
		case 10: return AnvilDamageType.HEAVY_DAMAGED;
		case 11: return AnvilDamageType.HEAVY_DAMAGED;
		default: return AnvilDamageType.NON_DAMAGED;
		}
	}

	public void setAnvilDamageType(AnvilDamageType type) {
		Anvil anvil = (Anvil) block.getState();
		if(type == AnvilDamageType.NON_DAMAGED) {
			block.setType(Material.ANVIL);
		} else if(type == AnvilDamageType.SLIGHT_DAMAGED) {
			block.setType(Material.CHIPPED_ANVIL);
		} else if(type == AnvilDamageType.HEAVY_DAMAGED) {
			block.setType(Material.DAMAGED_ANVIL);
		}
	}
}
