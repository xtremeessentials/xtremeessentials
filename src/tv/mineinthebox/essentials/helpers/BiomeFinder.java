package tv.mineinthebox.essentials.helpers;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.interfaces.XPlayer;

public class BiomeFinder {

	private xEssentials pl;
	
	public BiomeFinder(xEssentials pl) {
		this.pl = pl;
	}
	
	/**
	 * this method is used within the /biome-tp command
	 * 
	 * @author xize
	 * @param p - the player
	 * @param biome - the biome which we need to find
	 * @param chunk - the current chunk of the player
	 * @param range - the range per chunks
	 */
	public void searchBiome(Player p, Biome biome, int range) {

		Chunk chunk = p.getLocation().getChunk();
		
		int X = chunk.getX();
		int Z = chunk.getZ();

		//this should be theoretic create a circle around the player with cos and sin (this is being taken from my vanish code)
		//then we force load the chunk to avoid NPE's
		//if we found a chunk with the biome we want we teleport the player to it and set force load off
		//when force loading chunks is off we fall back on vanilla chunk loading.
		//we use a range as defined in the parameters of this method

		for(double i = 0.0; i < 360.0; i += 20.0) {
			double angle = (i*Math.PI / 180);
			int x = (int) (range/2*Math.cos(angle));
			int z = (int) (range/2*Math.sin(angle));
			X = X+x;
			Z = Z+z;
			Chunk c = chunk.getWorld().getChunkAt(X, Z);
			boolean bol = c.load(true);
			if(bol) {
				//tempory force load the chunk to avoid NPE and other stacktraces which contribute that the chunk is null (unloaded).
				c.setForceLoaded(true);
				Biome a = c.getWorld().getBiome(c.getX(), c.getZ());
				p.sendMessage(ChatColor.DARK_GREEN + "[Biome-TP]: " + ChatColor.GRAY+"seeing biome in radius "+ChatColor.RED+a.name());
				
				if(p.getWorld().getBiome(X, Z).equals(biome)) {
					Block b = c.getWorld().getHighestBlockAt(X, Z);
					if(b.getType().isBlock()) {
						p.sendMessage(ChatColor.DARK_GREEN + "[Biome-TP]: "+ChatColor.GRAY + "teleporting to "+ChatColor.DARK_GREEN+biome.name()+"!");
						p.teleport(b.getLocation().add(0, 1, 0));
						
						//get XPlayer and cancel task.
						XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
						xp.removeBiomeTP();
						
						return;
					} else {
						//get XPlayer and cancel task.
						XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
						xp.removeBiomeTP();
						
						p.sendMessage(ChatColor.DARK_GREEN + "[Biome-TP]: "+ChatColor.GRAY + "found biome "+biome.name()+" but the block is not solid!, coords: X: "+b.getLocation().add(0, 1, 0).getX()+", Y: "+b.getLocation().add(0,1,0).getY()+", Z: "+b.getLocation().add(0,1,0).getZ());
						return;
					}
				}
				c.setForceLoaded(false);
			}
		}
		p.sendMessage(ChatColor.DARK_GREEN+"[Biome-TP]: "+ChatColor.RED + "could not find the specified biome, retrying!");
	}
}
