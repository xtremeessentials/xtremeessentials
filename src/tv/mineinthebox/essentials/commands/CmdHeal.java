package tv.mineinthebox.essentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;

public class CmdHeal extends CommandTemplate {

	public CmdHeal(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
	}

	@SuppressWarnings("deprecation")
	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("heal")) {
			if(sender.hasPermission(PermissionKey.CMD_HEAL.getPermission())) {
				if(args.length == 0) {
					if(sender instanceof Player) {
						Player p = (Player) sender;
						p.setHealth(20);
						sendMessage("setting health to 20!");
					} else {
						getWarning(WarningType.PLAYER_ONLY);
					}
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						showHelp();
					} else {
						Player p = Bukkit.getPlayer(args[0]);
						if(p instanceof Player) {
							sendMessage("setting health to 20 for player "+p.getName()+"!");
							p.sendMessage(ChatColor.DARK_GREEN + "[heal]: "+ChatColor.GRAY+sender.getName() + " has set your health to 20!");
							p.setHealth(20);
						} else {
							sendMessage(ChatColor.RED + "this player is not online!");
						}
					}
				}
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}
	
	@Override
	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[heal help]___Oo.");
		sender.sendMessage(ChatColor.GRAY + "/heal - heals a player");
		sender.sendMessage(ChatColor.GRAY + "/heal <player> - heals a other player");
	}
}
