package tv.mineinthebox.essentials.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.helpers.BiomeFinder;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;
import tv.mineinthebox.essentials.interfaces.XPlayer;

public class CmdBiomeTP extends CommandTemplate {

	public CmdBiomeTP(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
	}

	private final int DEFAULT_RANGE = 100;

	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("biome-tp")) {

			if(sender.hasPermission(PermissionKey.IS_ADMIN.getPermission())) {
				if(sender instanceof Player) {
					if(args.length == 0) {
						showHelp();
					} else if(args.length == 1) {
						if(args[0].equalsIgnoreCase("help")) {
							showHelp();
						} else if(args[0].equalsIgnoreCase("cancel")) {
							if(sender instanceof Player) {
								Player p = (Player)sender;
								XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
								if(xp.hasBiomeTP()) {
									xp.removeBiomeTP();
									sendMessage("canceled biome-tp!");
								} else {
									sendMessage(ChatColor.RED + "you don't have any biome-tp open!");
								}
							} else {
								getWarning(WarningType.PLAYER_ONLY);
							}
						} else if(args[0].equalsIgnoreCase("list")) {
							StringBuilder biomes = new StringBuilder(", ");
							for(Biome b : Biome.values()) {
								biomes.append(", ");
								biomes.append(b.name());
							}
							sendMessage(ChatColor.GRAY + "applicable biomes:");
							sendMessage(ChatColor.GRAY + biomes.toString());
						} else {
							if(sender instanceof Player) {
								Player p = (Player) sender;
								try {
									XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
									if(xp.hasBiomeTP()) {
										sendMessage(ChatColor.RED + "it seems you are already searching to teleport to a biome, type /biome-tp cancel to cancel it!");
										return false;
									}
									Biome b = Biome.valueOf(args[0].toUpperCase());
									BukkitTask t = findBiome(p, b, DEFAULT_RANGE);
									xp.setBiomeTP(t);
									sender.sendMessage(ChatColor.DARK_GREEN + "[Biome-TP]: "+ChatColor.GRAY + "searching biome "+b.name().toLowerCase()+" in a radius of "+DEFAULT_RANGE+" chunks!");
								} catch(IllegalArgumentException e) {
									sendMessage(ChatColor.RED + "biome is invalid!");
								}
							} else {
								getWarning(WarningType.PLAYER_ONLY);
							}
						}
					} else if(args.length == 2) {
						if(sender instanceof Player) {
							Player p = (Player) sender;
							try {
								XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(p.getName());
								if(xp.hasBiomeTP()) {
									sendMessage(ChatColor.RED + "it seems you are already searching to teleport to a biome, type /biome-tp cancel to cancel it!");
									return false;
								}
								int range = Integer.parseInt(args[1]);
								Biome b = Biome.valueOf(args[0].toUpperCase());
								BukkitTask t = findBiome(p, b, range);
								xp.setBiomeTP(t);
								sender.sendMessage(ChatColor.DARK_GREEN + "[Biome-TP]: "+ChatColor.GRAY + "searching biome "+b.name().toLowerCase()+" in a radius of "+DEFAULT_RANGE+" chunks!");
							} catch(NumberFormatException e) {
								sendMessage(ChatColor.RED + "cannot parse argument 2 as a number!");
							} catch(IllegalArgumentException e) {
								sendMessage(ChatColor.RED + "biome is invalid!");
							}
						} else {
							getWarning(WarningType.PLAYER_ONLY);
						}
					}
				} else {
					getWarning(WarningType.PLAYER_ONLY);
				}		
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}

		}
		return false;
	}

	private BukkitTask findBiome(final Player p, final Biome b, final int range) {
		Bukkit.broadcastMessage(ChatColor.RED + p.getName() + " has activated biome teleport, this might cause lag!");
		
		final BiomeFinder finder = new BiomeFinder(this.pl);

		final BukkitTask t = new BukkitRunnable() {

			@Override
			public void run() {
				finder.searchBiome(p, b, 100);
			}

		}.runTaskTimer(pl, 1, 800L);
		return t;
	}

	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[Biome-TP]___Oo.");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/biome-tp " + ChatColor.WHITE + ": shows help");
		sender.sendMessage(ChatColor.RED + "please note: generating chunks is not async this means this command may crash your server keep the ranges small!");
		sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/biome-tp list" + ChatColor.WHITE + ": shows a list of applicable biomes");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/biome-tp <biome> " + ChatColor.WHITE + ": tries to find a biome and tries to teleport in a range of 100 chunks");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/biome-tp <biome> <range> " + ChatColor.WHITE + ": tries to find a biome and tries to teleport to a specific range");
		sender.sendMessage(ChatColor.RED + "Admin: "+ ChatColor.GRAY + "/biome-tp cancel " + ChatColor.WHITE + ": cancels searching!");
	}

	public List<String> onTabComplete(CommandSender sender, Command cmd, String[] args) {
		if(args.length == 1) {

			List<String> list = new ArrayList<String>();
			list.add("help");
			list.add("cancel");
			list.add("list");

			StringBuilder build = new StringBuilder();
			for(Biome b : Biome.values()) {
				if(build.length() == 0) {
					build.append(b.name());
				}
				build.append(",");
				build.append(b.name());
			}
			list.addAll(Arrays.asList(build.toString().split(",")));
			return list;
		}
		return null;
	}

}
