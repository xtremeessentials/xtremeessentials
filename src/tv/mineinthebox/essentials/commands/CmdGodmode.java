package tv.mineinthebox.essentials.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;
import tv.mineinthebox.essentials.interfaces.XOfflinePlayer;
import tv.mineinthebox.essentials.interfaces.XPlayer;

public class CmdGodmode extends CommandTemplate {

	public CmdGodmode(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
	}

	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("godmode")) {

			if(sender.hasPermission(PermissionKey.CMD_GODMODE.getPermission())) {
				if(args.length == 0) {
					if(sender instanceof Player) {
						XPlayer p = pl.getManagers().getPlayerManager().getPlayer(sender.getName());
						if(p.isGodmode()) {
							sendMessage("godmode disabled!");
							p.setGodmode(false);	
						} else {
							sendMessage("godmode enabled!");
							p.setGodmode(true);
						}
					} else {
						getWarning(WarningType.PLAYER_ONLY);
					}
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						showHelp();
					} else {
						XPlayer xp = pl.getManagers().getPlayerManager().getPlayer(args[0]);
						if(xp != null) {
							if(xp.isGodmode()) {
								sendMessage("godmode disabled for player "+xp.getName()+"!");
								xp.sendMessage(ChatColor.DARK_GREEN + "[godmode]: ", ChatColor.GRAY + sender.getName() + " has disabled godmode for you!");
								xp.setGodmode(false);	
							} else {
								sendMessage("godmode enabled for player "+xp.getName()+"!");
								xp.sendMessage(ChatColor.DARK_GREEN + "[godmode]: ", ChatColor.GRAY + sender.getName() + " has enabled godmode for you!");
								xp.setGodmode(true);
							}
						} else {
							sendMessage(ChatColor.RED + "no player found with that name!");
						}
					}
				}

			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}

	@Override
	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[godmode help]___Oo.");
		sender.sendMessage(ChatColor.GRAY + "/godmode - toggles godmode");
		sender.sendMessage(ChatColor.GRAY + "/godmode <player> - toggles godmode for a different player");
	}

	private List<String> getPlayerByName(String p) {
		List<String> s = new ArrayList<String>();
		for(XOfflinePlayer name : pl.getManagers().getPlayerManager().getOfflinePlayers()) {
			if(name.getName().toUpperCase().startsWith(p.toUpperCase())) {
				s.add(name.getName());
			}
		}
		return s;
	}

	public List<String> onTabComplete(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("godmode")) {
			if(args.length == 1) {
				if(sender.hasPermission(PermissionKey.CMD_GODMODE.getPermission())) {
					List<String> list = getPlayerByName(args[0]);
					return list;
				}
			}
		}
		return null;
	}

}
