package tv.mineinthebox.essentials.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;

public class CmdHunger extends CommandTemplate {

	public CmdHunger(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
	}

	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("hunger")) {
			if(sender.hasPermission(PermissionKey.CMD_HUNGER.getPermission())) {
				if(args.length == 0) {
					if(sender instanceof Player) {
						Player p = (Player) sender;
						p.setFoodLevel(20);
						sendMessage("food has been restored to full!");
					} else {
						getWarning(WarningType.PLAYER_ONLY);
					}
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						showHelp();
					} else {
						@SuppressWarnings("deprecation")
						Player p  = Bukkit.getPlayer(args[0]);
						if(p instanceof Player) {
							p.setFoodLevel(20);
							sendMessage("you restored "+p.getName()+" his food level to full!");
							p.sendMessage(ChatColor.DARK_GREEN + "[Hunger]: "+ChatColor.GRAY+ sender.getName() + " has restored your food level to full!");
						} else {
							sendMessage(ChatColor.RED + "player does not exist!");
						}
					}
				}
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}

	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[hunger]___Oo.");
		sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/hunger help "+ChatColor.WHITE+": shows help");
		sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/hunger "+ChatColor.WHITE+": fills hunger bar to 100%");
		sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/hunger <player> "+ChatColor.WHITE+": fills hunger bar to 100%");
	}

}
