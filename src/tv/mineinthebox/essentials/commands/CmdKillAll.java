package tv.mineinthebox.essentials.commands;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;

public class CmdKillAll extends CommandTemplate {

	public CmdKillAll(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
	}

	private int RADIUS = 50;

	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("killall")) {
			if(sender.hasPermission(PermissionKey.CMD_KILL_ALL.getPermission())) {
				if(args.length == 0) {
					if(sender.hasPermission(PermissionKey.CMD_KILL_ALL_PLAYERS.getPermission())) {
						sendMessage("killing all the players!");
						for(Player p : Bukkit.getOnlinePlayers()) {
							if(!p.hasPermission(PermissionKey.IS_ADMIN.getPermission())) {
								p.setHealth(0);
							}
						}
					} else {
						getWarning(WarningType.NO_PERMISSION);
					}
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						showHelp();
					} else if(args[0].equalsIgnoreCase("living") || args[0].equalsIgnoreCase("entities") || args[0].equalsIgnoreCase("animals")) { //alias
						if(sender.hasPermission(PermissionKey.CMD_KILL_ALL_ANIMALS.getPermission())) {
							if(sender instanceof Player) {
								Player p = (Player)sender;
								sendMessage("killing all animals around you within 25 blocks range");
								int amount = 0;
								Collection<Entity> entities = p.getWorld().getNearbyEntities(p.getLocation(), RADIUS/2, 200, RADIUS/2);
								for(Entity entity : entities) {
									if(entity instanceof Animals) {
										if(entity.getCustomName() == null) {
											((Animals) entity).remove();
											amount++;
										}
									}
								}
								sendMessage("done, in total "+amount+" of animal entities have been killed!");
							} else {
								getWarning(WarningType.PLAYER_ONLY);
							}
						} else {
							getWarning(WarningType.NO_PERMISSION);
						}
					} else if(args[0].equalsIgnoreCase("monsters") || args[0].equalsIgnoreCase("monster")) {
						if(sender.hasPermission(PermissionKey.CMD_KILL_ALL_MONSTERS.getPermission())) {
							if(sender instanceof Player) {
								Player p = (Player)sender;
								sendMessage("killing all monsters around you within 25 blocks range");
								int amount = 0;
								Collection<Entity> entities = p.getWorld().getNearbyEntities(p.getLocation(), RADIUS/2, 200, RADIUS/2);
								for(Entity entity : entities) {
									if(entity instanceof Monster) {
										if(entity.getCustomName() == null) {
											((Monster) entity).remove();
											amount++;
										}
									}
								}
								sendMessage("done, in total "+amount+" of monster entities have been killed!");
							} else {
								getWarning(WarningType.PLAYER_ONLY);
							}
						} else {
							getWarning(WarningType.NO_PERMISSION);
						}
					}
				}
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}

	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[killall]___Oo.");
		sender.sendMessage(ChatColor.GRAY + "if you don't see any commands below, please visit our wiki!");
		sender.sendMessage("https://gitlab.com/xtremeessentials/xtremeessentials/wikis/commands%20and%20permissions");
		sender.sendMessage("\n");
		if(sender.hasPermission(PermissionKey.CMD_KILL_ALL_PLAYERS.getPermission())) {
			sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/killall "+ChatColor.WHITE + ": kills all the players except the administrators");
		}
		if(sender.hasPermission(PermissionKey.CMD_KILL_ALL_ANIMALS.getPermission())) {
			sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/killall living|enitites|animals "+ChatColor.WHITE+": kills all the living entities");
		}
		if(sender.hasPermission(PermissionKey.CMD_KILL_ALL_MONSTERS.getPermission())) {
			sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/killall monster|monsters "+ChatColor.WHITE+": kills all the monsters");
		}
	}

	public List<String> onTabComplete(CommandSender sender, Command cmd, String[] args) {
		if(args.length == 1) {
			List<String> list = Arrays.asList(new String[]{
					"help",
					"living",
					"entities",
					"animals",
					"monster",
					"monsters"
			});
			return list;
		}
		return null;
	}

}
