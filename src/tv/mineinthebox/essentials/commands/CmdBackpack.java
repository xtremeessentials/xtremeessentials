package tv.mineinthebox.essentials.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.instances.Backpack;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;

public class CmdBackpack extends CommandTemplate {

	private final xEssentials pl;

	public CmdBackpack(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
		this.pl = pl;
	}

	private List<String> getContainedMaterials(String material) {
		List<String> list = new ArrayList<String>();
		for(String mat : pl.getConfiguration().getMaterials()) {
			if(mat.startsWith(material.toUpperCase())) {
				list.add(mat);
			}
		}
		return list;
	}

	public List<String> onTabComplete(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("backpack")) {
			if(sender.hasPermission(PermissionKey.CMD_BACKPACK.getPermission())) {
				if(args.length == 2) {
					List<String> list = getContainedMaterials(args[1]);
					return list;
				} else if(args.length == 3) {
					List<String> list = getContainedMaterials(args[1]);
					return list;
				}	
			}
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("backpack")) {
			if(sender.hasPermission(PermissionKey.CMD_BACKPACK.getPermission())) {
				if(args.length == 0) {
					if(sender.hasPermission(PermissionKey.IS_ADMIN.getPermission())) {
						showHelp();
					} else {
						sendMessage("you dont have permission!");
					}
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						if(sender.hasPermission(PermissionKey.IS_ADMIN.getPermission())) {
							showHelp();
						} else {
							sendMessage("you dont have permission!");
						}
					} else {
						sendMessage("we don't know much about this argument!");
					}
				} else if(args.length == 2) {
					try {
						Player p = Bukkit.getPlayer(args[0]);
						if(p instanceof Player) {
								Material mat = Material.getMaterial(args[1].toUpperCase());
								ItemStack item = new ItemStack(mat, 1);
								Backpack backpack = pl.getManagers().getBackPackManager().createBackpack(item.getType());
								try {
									p.getInventory().addItem(backpack.getBackPackItem());
									sendMessageTo(p, "you retrieved items from " + sender.getName());
								} catch(IllegalArgumentException e) {
									sendMessage(p.getName() + " is to full");
								}
						} else {
							sendMessage("this player is not online!");
						}
					} catch(NullPointerException e) {
						sendMessage("invalid item!");
					}
				}
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}

	@Override
	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[backpack help]___Oo.");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/backpack " + ChatColor.WHITE + ": shows help");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/backpack help " + ChatColor.WHITE + ": shows help");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/backpack <player> <item> " + ChatColor.WHITE + ": adds a backpack on a item.");
	}
}
