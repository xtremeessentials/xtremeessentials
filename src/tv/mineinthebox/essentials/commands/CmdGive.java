package tv.mineinthebox.essentials.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;

public class CmdGive extends CommandTemplate {

	private final xEssentials pl;

	public CmdGive(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
		this.pl = pl;
	}

	private List<String> getContainedMaterials(String material) {
		List<String> list = new ArrayList<String>();
		for(String mat : pl.getConfiguration().getMaterials()) {
			if(mat.startsWith(material.toUpperCase())) {
				list.add(mat);
			}
		}
		return list;
	}

	public List<String> onTabComplete(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("give")) {
			if(sender.hasPermission(PermissionKey.CMD_GIVE.getPermission())) {
				if(args.length == 2) {
					List<String> list = getContainedMaterials(args[1]);
					return list;
				} else if(args.length == 3) {
					List<String> list = getContainedMaterials(args[1]);
					return list;
				}	
			}
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("give")) {
			if(sender.hasPermission(PermissionKey.CMD_GIVE.getPermission())) {
				if(args.length == 0) {
					showHelp();
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						showHelp();
					} else {
						sendMessage("we don't know much about this argument!");
					}
				} else if(args.length == 2) {
					Player p = Bukkit.getPlayer(args[0]);
					if(p instanceof Player) {
						ItemStack item = null;
						try {
							Material mat = Material.getMaterial(args[1].toUpperCase());
							item = new ItemStack(mat);
						} catch(IllegalArgumentException e) {
							sendMessage("invalid item!");
						}

						try {
							p.getInventory().addItem(item);
							sendMessageTo(p, "you retrieved items from " + sender.getName());
						} catch(IllegalArgumentException e) {
							sendMessage(p.getName() + " is to full");
						}
					} else {
						sendMessage("this player is not online!");
					}
				} else if(args.length == 3) {
					Player p = Bukkit.getPlayer(args[0]);
					if(p instanceof Player) {
						ItemStack item = null;
						try {
							Material mat = Material.getMaterial(args[1].toUpperCase());
							item = new ItemStack(mat);
						} catch(IllegalArgumentException e) {
							sendMessage("invalid item!");
						}

						if(!isNumberic(args[2]) && Integer.parseInt(args[2]) <= 64) {
							sendMessage("not a valid amount!");
							return false;
						}
						
						item.setAmount(Integer.parseInt(args[2]));
						
						try {
							p.getInventory().addItem(item);
							sendMessageTo(p, "you retrieved items from " + sender.getName());
						} catch(IllegalArgumentException e) {
							sendMessage(p.getName() + " is to full");
						}
					} else {
						sendMessage("this player is not online!");
					}
				}
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}

	private Boolean isNumberic(String s) {
		try {
			Integer i = Integer.parseInt(s);
			if(i != null) {
				return true;
			}
		} catch(NumberFormatException e) {

		}
		return false;
	}

	@Override
	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[give help]___Oo.");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/give " + ChatColor.WHITE + ": shows help");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/give help " + ChatColor.WHITE + ": shows help");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/give <player> <item:subdata> " + ChatColor.WHITE + ": gives the player one item");
		sender.sendMessage(ChatColor.RED + "Admin: " + ChatColor.GRAY + "/give <player> <item:subdata> <amount> " + ChatColor.WHITE + " gives the player a item with a custom amount!");
	}
}
