package tv.mineinthebox.essentials.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.StructureType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tv.mineinthebox.essentials.xEssentials;
import tv.mineinthebox.essentials.enums.PermissionKey;
import tv.mineinthebox.essentials.helpers.StructureFinder;
import tv.mineinthebox.essentials.interfaces.CommandTemplate;

public class CmdStructure extends CommandTemplate {

	public CmdStructure(xEssentials pl, Command cmd, CommandSender sender) {
		super(pl, cmd, sender);
	}

	private final StructureFinder finder = new StructureFinder();
	
	public boolean execute(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("structure")) {
			if(sender.hasPermission(PermissionKey.CMD_STRUCTURE.getPermission())) {
				if(args.length == 0) {
					showHelp();
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						showHelp();
					}
				} else if(args.length == 2) {
					Player p = Bukkit.getPlayer(args[0]);
					String format = formatName(args[1]);
					if(p instanceof Player) {
						try {
							sendMessage("teleporting "+p.getName()+" to the nearest "+ format + " from his nearest location!");
							if(!sender.getName().equals(p.getName())) {
								p.sendMessage(ChatColor.DARK_GREEN + "[Structure]: "+ChatColor.GRAY+sender.getName()+" teleported you to the nearest "+format+"!");
							}
							p.teleport(finder.getStructure(p.getLocation(), format));
							p.playSound(p.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 1F, 1F);
						} catch (Exception e) {
							if(!sender.getName().equals(p.getName())) {
								p.sendMessage(ChatColor.DARK_GREEN + "[Structure]: "+ChatColor.RED+"teleport has been failed to "+format+" maybe wrong structure given in by the admin or unsupported minecraft server version");
							}
							sendMessage("teleporting to "+format+" has been failed either because the name is wrong or this version of minecraft is unsupported!");
						}
					} else {
						sendMessage(ChatColor.RED + "this player is not online!");
					}
				}
			} else {
				getWarning(WarningType.NO_PERMISSION);
			}
		}
		return false;
	}

	private String formatName(String arg) {
		return arg.substring(0, 1).toUpperCase()+arg.substring(1, arg.length());
	}
	
	public void showHelp() {
		sender.sendMessage(ChatColor.GOLD + ".oO___[structure]___Oo.");
		sender.sendMessage(ChatColor.GRAY + "/structure <player> <structure> - teleports a player to a nearby structure");
	}

	public List<String> onTabComplete(CommandSender sender, Command cmd, String[] args) {
		if(cmd.getName().equalsIgnoreCase("structure")) {
			if(args.length == 2) {
				List<String> structures = new ArrayList<String>();
				for(StructureType b : StructureType.getStructureTypes().values()) {

					if(b.getName().startsWith(args[1])) {
						structures.add(b.getName());
					}

				}
				return structures;
			}
		}
		return null;
	}

}
